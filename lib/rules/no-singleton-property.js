/* Local Variables:  */
/* create-lockfiles: nil */
/* End:              */
"use strict";
// copied from http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#JavaScript
function getEditDistance(a, b) {
  if(a.length === 0) return b.length; 
  if(b.length === 0) return a.length; 
 
  var matrix = [];
 
  // increment along the first column of each row
  var i;
  for(i = 0; i <= b.length; i++){
    matrix[i] = [i];
  }
 
  // increment each column in the first row
  var j;
  for(j = 0; j <= a.length; j++){
    matrix[0][j] = j;
  }
 
  // Fill in the rest of the matrix
  for(i = 1; i <= b.length; i++){
    for(j = 1; j <= a.length; j++){
      if(b.charAt(i-1) == a.charAt(j-1)){
        matrix[i][j] = matrix[i-1][j-1];
      } else {
        matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
                                Math.min(matrix[i][j-1] + 1, // insertion
                                         matrix[i-1][j] + 1)); // deletion
      }
    }
  }
 
  return matrix[b.length][a.length];
}

var is_similar = function (s1, s2) {
    return  s1 !== s2 &&
        getEditDistance(s1, s2) / s1.length < 1/4;
};

module.exports = function (context) {
    var d_property_nodes = {};
    var add_property_node = function (p, n) {
        if (p in d_property_nodes) {
            d_property_nodes[p].push(n);
        } else {
            d_property_nodes[p] = [n];
        }
    };

    return {
        Program: function () {
            d_property_nodes = {};
        },

        Property: function (n) {
            add_property_node(n.key.name, n);
        },

        MemberExpression: function (n) {
            add_property_node(n.property.name, n);
        },

        'Program:exit': function () {
            var properties = Object.keys(d_property_nodes);
            properties.forEach(function (p) {
                if (d_property_nodes[p].length > 1) {
                        return;
                }
                var s = properties.filter(is_similar.bind(null, p));

                if (s.length !== 0) {
                    context.report(d_property_nodes[p][0],
                                   'Is "'+p+'" misspelled "'+s[0]+'"?');
                }
            });
        }
    };
};
